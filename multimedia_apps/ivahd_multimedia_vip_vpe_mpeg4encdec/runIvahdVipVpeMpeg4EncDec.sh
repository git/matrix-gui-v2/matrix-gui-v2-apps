#!/bin/sh

machine_type="`cat /etc/hostname`"

vip_node=$(find /sys/class/video4linux/video* -print -exec cat {}/name \; | grep -B1 vip | grep -o 'video[0-9]*$')

echo ""
echo "Run VIP, VPE, Ducati MPEG4 Encode, Ducati MPEG4 Decode and Display"
echo ""

gst-launch-1.0 -e v4l2src device=/dev/$vip_node num-buffers=1000 io-mode=4 ! 'video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1' ! vpe num-input-buffers=8 ! 'video/x-raw, format=(string)NV12, width=(int)800, height=(int)480' !  queue ! ducatimpeg4enc bitrate=4000 ! queue !  mpeg4videoparse ! queue ! ducatimpeg4dec ! queue ! kmssink
