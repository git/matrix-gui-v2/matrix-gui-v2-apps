#!/bin/sh

amixer_find="/usr/bin/amixer"
if [ ! -f $amixer_find ]; then
        echo "amixer not found"
        echo "Please connect audio output and install ALSA soundcard driver"
elif grep -q "no soundcards" /proc/asound/cards; then
        echo "No sound devices found!"
else
	machine_type="`cat /etc/hostname`"
	platform="default"
	if [ "$machine_type" = "am335x-evm" ] || [ "$machine_type" = "am335x-hs-evm" ]; then
		platform="am35xx"
		resolution="`fbset | awk '/geometry/ {print $2"x"$3}'`"
		if [ "$resolution" = "480x272" ]; then
			filename="/usr/share/ti/video/TearOfSteel-AV-Short-400x240.mp4"
		else
			# Use WVGA for all other resolutions
			filename="/usr/share/ti/video/TearOfSteel-AV-Short-720x420.mp4"
		fi
	elif [ "$machine_type" = "dra7xx-evm" ]
	then
		filename="/usr/share/ti/video/TearOfSteel-AV-Short-720x420.mp4"
	elif [ "$machine_type" = "omap5-evm" ]
	then
		filename="/usr/share/ti/video/TearOfSteel-AV-Short-720x406.mp4"
	elif [ "$machine_type" = "am65xx-evm" ]
	then
		platform="am65xx"
		filename="/usr/share/ti/video/TearOfSteel-AV-Short-720x406.mp4"
	elif [ "$machine_type" = "am437x-evm" ] || [ "$machine_type" = "am437x-hs-evm" ] || [ "$machine_type" = "am438x-epos-evm" ]
	then
		resolution="`fbset | awk '/geometry/ {print $2"x"$3}'`"
		if [ "$resolution" = "480x272" ]; then
			filename="/usr/share/ti/video/TearOfSteel-AV-Short-400x240.mp4"
		else
			filename="/usr/share/ti/video/TearOfSteel-AV-Short-720x406.mp4"
		fi
	else
		default_display="`cat /sys/devices/platform/omapdss/manager0/display`"
		if [ "$default_display" = "dvi" ]; then
			if [ "$machine_type" = "beagleboard" ]; then
				filename="/usr/share/ti/video/TearOfSteel-AV-Short-640x360.mp4"
			else
				filename="/usr/share/ti/video/TearOfSteel-AV-Short-720x406.mp4"
			fi
		else
			if [ "$machine_type" = "am37x-evm" ]; then
				filename="/usr/share/ti/video/HistoryOfTIAV-VGA-r.mp4"
			elif [ "$machine_type" = "am3517-evm" ]; then
				filename="/usr/share/ti/video/TearOfSteel-AV-Short-400x240.mp4"
			fi
		fi
	fi
	if [ "$machine_type" = "am37x-evm" ]; then
		amixer cset name='HeadsetL Mixer AudioL1' on
		amixer cset name='HeadsetR Mixer AudioR1' on
		amixer -c 0 set Headset 1+ unmute
	elif [ "$machine_type" = "am335x-evm" ] || [ "$machine_type" = "am335x-hs-evm" ]; then
		amixer cset name='PCM Playback Volume' 127
	elif [ "$machine_type" = "omap5-evm" ]; then
		amixer cset name='PCM Playback Volume' 127
	elif [ "$machine_type" = "am437x-evm" ] || [ "$machine_type" = "am437x-hs-evm" ] || [ "$machine_type" = "am438x-epos-evm" ]; then
		# EPOS uses a different configuration so check if
		# we are running on that board
		model_name=`cat /proc/device-tree/model | grep -i epos`
		if [ "$?" = '0' ]; then
			amixer cset name='DAC Playback Volume' 127
			amixer cset name='HP Analog Playback Volume' 66
			amixer cset name='HP Driver Playback Switch' on
			amixer cset name='HP Left Switch' on
			amixer cset name='HP Right Switch' on
			amixer cset name='Output Left From Left DAC' on
			amixer cset name='Output Right From Right DAC' on
		else
			amixer cset name='PCM Playback Volume' 127
		fi
	fi
	if [ "$platform" = "am35xx" ]; then
		gst-launch-1.0 filesrc location=$filename ! queue ! qtdemux name=demux demux.audio_0 ! queue ! faad ! alsasink demux.video_0 ! queue ! avdec_mpeg4 ! videoconvert ! fbdevsink device=/dev/fb0
	elif [ "$platform" = "am65xx" ]; then
		gst-launch-1.0 filesrc location=$filename ! queue ! qtdemux name=demux demux.audio_0 ! queue ! faad ! alsasink demux.video_0 ! queue ! avdec_mpeg4 ! videoconvert ! 'video/x-raw, format=(string)BGRA' ! kmssink
	else
		gst-launch-1.0 filesrc location=$filename ! queue ! qtdemux name=demux demux.audio_0 ! queue ! faad ! alsasink demux.video_0 ! queue ! avdec_mpeg4 ! videoconvert ! kmssink
	fi
fi
