#!/bin/sh

machine_type="`cat /etc/hostname`"

input_filename="/usr/share/ti/video/TearOfSteel-Short-1280x720.265"
output_filename="/usr/share/ti/video/TearOfSteel-Short-640x480_DSP_Processed.mp4"

if [ ! -f $filename ]; then
        echo "Video clip not found"
        exit 1
fi
echo ""
echo "Launch GStreamer pipeline"
echo ""

gst-launch-1.0 filesrc location=$input_filename ! 'video/x-raw, format=(string)NV12, framerate=(fraction)24/1, width=(int)1280, height=(int)720'  ! queue ! h265dec threads=1 ! videoconvert ! queue ! dsp66videokernel kerneltype=1 filtersize=9 lum-only=1 ! queue ! videoscale ! 'video/x-raw, width=(int)640, height=(int)480' ! avenc_mpeg4 ! mpeg4videoparse ! mpegtsmux ! queue ! filesink location=$output_filename
