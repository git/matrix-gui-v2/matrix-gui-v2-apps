#!/bin/sh

amixer_find="/usr/bin/amixer"
if [ ! -f $amixer_find ]; then
    echo "amixer not found"
    echo "Please connect audio output and install ALSA soundcard driver"
    exit
elif grep -q "no soundcards" /proc/asound/cards; then
    echo "No sound devices found!"
    exit
else
    machine_type="`cat /etc/hostname`"
    filename=$( mktemp )

    echo ""

    resolution="`fbset | awk '/geometry/ {print $2"x"$3}'`"
    AUDIO_CARD=0
    if [ "$machine_type" = "am37x-evm" ]; then
        if [ "$resolution" = "480x272" ]; then
           echo "No sound input device is available on this EVM."
           exit
        fi
        amixer cset name='HeadsetL Mixer AudioL1' on > /dev/null
        amixer cset name='HeadsetR Mixer AudioR1' on > /dev/null
        amixer -c 0 set Headset 1+ unmute > /dev/null
        amixer cset name='Analog Left AUXL Capture Switch' 1  > /dev/null
        amixer cset name='Analog Right AUXR Capture Switch' 1 > /dev/null
        amixer sset 'Headset' 60%,60% > /dev/null
    elif [ "$machine_type" = "am335x-evm" ] || [ "$machine_type" = "am335x-hs-evm" ]; then
        amixer cset name='PCM Playback Volume' 80%,80% > /dev/null
        amixer cset name='PGA Capture Volume' 65%,65% > /dev/null
        amixer sset 'Right PGA Mixer Line1R' on > /dev/null
        amixer sset 'Right PGA Mixer Line1L' on > /dev/null
        amixer sset 'Left PGA Mixer Line1R' on > /dev/null
        amixer sset 'Left PGA Mixer Line1L' on > /dev/null
        amixer sset 'Left Line1R Mux' differential > /dev/null
        amixer sset 'Right Line1L Mux' differential > /dev/null
    elif [ "$machine_type" = "am57xx-evm" ] || [ "$machine_type" = "am57xx-hs-evm" ]; then
        # Exit for AM5 IDK
        if cat /proc/device-tree/model | grep "IDK"
        then
            echo "Exit as there is no audio port"
            exit 1
        fi
        # detect audio card for AM5 GP EVM
        AUDIO_CARD=$(aplay -l | grep -i -v hdmi | grep -i -o 'card [0-9]*' | grep -o [0-9])

        amixer cset -c 1 name='PCM Playback Volume' 127
        #line out
        amixer -c BeagleBoardX15 sset 'Left Line Mixer DACL1' on
        amixer -c BeagleBoardX15 sset 'Right Line Mixer DACR1' on
        amixer -c BeagleBoardX15 sset 'Line DAC' 90
        # line in
        amixer -c BeagleBoardX15 sset 'Left PGA Mixer Mic2L' on
        amixer -c BeagleBoardX15 sset 'Right PGA Mixer Mic2R' on
        amixer -c BeagleBoardX15 sset 'PGA' 40
    elif [ "$machine_type" = "am437x-evm" ] || [ "$machine_type" = "am437x-hs-evm" ] || [ "$machine_type" = "am438x-epos-evm" ]; then
        # EPOS uses a different configuration
        # we are running on that board
        model_name=`cat /proc/device-tree/model | grep -i epos`

        if [ "$?" = '0' ]; then
            amixer cset name='DAC Playback Volume' 127
            amixer cset name='HP Analog Playback Volume' 66
            amixer cset name='HP Driver Playback Switch' on
            amixer cset name='HP Left Switch' on
            amixer cset name='HP Right Switch' on
            amixer cset name='Output Left From Left DAC' on
            amixer cset name='Output Right From Right DAC' on
            #line in
            amixer sset 'MIC1RP P-Terminal' 'FFR 10 Ohm'
            amixer sset 'MIC1LP P-Terminal' 'FFR 10 Ohm'
            amixer sset 'ADC' 40
            amixer cset name='ADC Capture Switch' on
            amixer sset 'Mic PGA' 50
        else
            amixer cset name='PCM Playback Volume' 127
            if [ "$resolution" = "480x272" ]; then
                #line in
                amixer -c AM437xSKEVM sset 'Left PGA Mixer Line1L' on
                amixer -c AM437xSKEVM sset 'Right PGA Mixer Line1R' on
                amixer -c AM437xSKEVM sset 'Left PGA Mixer Mic3L' off
                amixer -c AM437xSKEVM sset 'Right PGA Mixer Mic3R' off
                amixer -c AM437xSKEVM sset 'PGA' 40
            else
                #line in
                amixer -c AM437xGPEVM sset 'Left PGA Mixer Line1L' on
                amixer -c AM437xGPEVM sset 'Right PGA Mixer Line1R' on
                amixer -c AM437xGPEVM sset 'Left PGA Mixer Mic3L' off
                amixer -c AM437xGPEVM sset 'Right PGA Mixer Mic3R' off
                amixer -c AM437xGPEVM sset 'PGA' 40
            fi
        fi
    elif [ "$machine_type" = "am65xx-evm" ]; then
        # Exit for AM65 for now until audio card info is available
            echo "Exit as there is no audio port"
            exit 1
    fi

    echo "Recording of 1000 buffers from Line-In will begin in 5 seconds..."
    sleep 5

    echo ""

    echo "Starting capture pipeline..."
    gst-launch-1.0 alsasrc device=hw:$AUDIO_CARD,0 num-buffers=1000 ! wavenc ! filesink location=$filename

    echo ""

    echo "Starting playback pipeline..."
    gst-launch-1.0 filesrc location=$filename ! wavparse ! alsasink device=hw:$AUDIO_CARD,0

    echo ""
    echo "Done."
fi

