#!/bin/sh

machine_type="`cat /etc/hostname`"
platform="default"
if [ "$machine_type" = "am335x-evm" ] || [ "$machine_type" = "am335x-hs-evm" ]; then
	platform="am35xx"
	resolution="`fbset | awk '/geometry/ {print $2"x"$3}'`"
	if [ "$resolution" = "480x272" ]; then
		filename="/usr/share/ti/video/TearOfSteel-Short-400x240.264"
	else
		# Use WVGA for all other resolutions
		filename="/usr/share/ti/video/TearOfSteel-Short-720x420.264"
	fi
elif [ "$machine_type" = "dra7xx-evm" ]
then
    filename="/usr/share/ti/video/TearOfSteel-Short-720x420.264"
elif [ "$machine_type" = "omap5-evm" ]
then
    filename="/usr/share/ti/video/TearOfSteel-Short-720x406.264"
elif [ "$machine_type" = "am437x-evm" ] || [ "$machine_type" = "am437x-hs-evm" ] || [ "$machine_type" = "am438x-epos-evm" ]
then
        resolution="`fbset | awk '/geometry/ {print $2"x"$3}'`"
        if [ "$resolution" = "480x272" ]; then
                filename="/usr/share/ti/video/TearOfSteel-Short-400x240.264"
        else
                filename="/usr/share/ti/video/TearOfSteel-Short-720x406.264"
        fi
elif [ "$machine_type" = "am65xx-evm" ]
then
     platform="am65xx"
     filename="/usr/share/ti/video/TearOfSteel-Short-720x406.264"
else
	default_display="`cat /sys/devices/platform/omapdss/manager0/display`"
	if [ "$default_display" = "dvi" ]; then
        	if [ "$machine_type" = "beagleboard" ]; then
                	filename="/usr/share/ti/video/TearOfSteel-Short-640x360.264"
        	else
                	filename="/usr/share/ti/video/TearOfSteel-Short-720x406.264"
        	fi
	else
        	if [ "$machine_type" = "am37x-evm" ]; then
                	filename="/usr/share/ti/video/HistoryOfTI-VGA-r.264"
        	elif [ "$machine_type" = "am3517-evm" ]; then
                	filename="/usr/share/ti/video/TearOfSteel-Short-400x240.264"
        	fi
	fi
fi
if [ ! -f $filename ]; then
        echo "Video clip not found"
        exit 1
fi
echo ""
echo "Launch GStreamer pipeline"
echo ""
echo "Length of video clip: 21 seconds"
if [ "$platform" = "am35xx" ]
then
	gst-launch-1.0 filesrc location=$filename ! queue ! h264parse ! avdec_h264 ! videoconvert ! fbdevsink device=/dev/fb0
elif [ "$platform" = "am65xx" ]
then
	gst-launch-1.0 filesrc location=$filename ! queue ! h264parse ! avdec_h264 ! videoconvert ! 'video/x-raw, format=(string)BGRA' ! kmssink
else
	gst-launch-1.0 filesrc location=$filename ! queue ! h264parse ! avdec_h264 ! videoconvert ! kmssink
fi