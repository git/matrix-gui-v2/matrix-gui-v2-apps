#!/bin/sh

machine_type="`cat /etc/hostname`"

filename="/usr/share/ti/video/TearOfSteel-Short-1920x800.mov"

AUDIO_SINK="alsasink"

if cat /proc/device-tree/model | grep "IDK"
then
  # Use fakesink for IDKs as they do not have audio ports
  AUDIO_SINK="fakesink"
else
  # Detect audio card for GP EVM
  AUDIO_CARD=$(aplay -l | grep -i -v hdmi | grep -i -o 'card [0-9]*' | grep -o [0-9])
  amixer cset -c $AUDIO_CARD name='Line Playback Switch' On
  amixer cset -c $AUDIO_CARD name='PCM Playback Volume' 127
  AUDIO_SINK="$AUDIO_SINK device=hw:$AUDIO_CARD,0"
fi

if [ ! -f $filename ]; then
        echo "Video clip not found"
        exit 1
fi

echo ""
echo "Run H.264 Decoding on IVAHD"
echo ""
gst-launch-1.0 playbin uri=file://$filename audio-sink="$AUDIO_SINK" video-sink="kmssink scale=true"
