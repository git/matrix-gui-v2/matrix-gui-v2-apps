#!/bin/sh

machine_type="`cat /etc/hostname`"

filename="/usr/share/ti/video/TearOfSteel-Short-1280x720.265"

if [ ! -f $filename ]; then
        echo "Video clip not found"
        exit 1
fi
echo ""
echo "Launch GStreamer pipeline"
echo ""

gst-launch-1.0 filesrc location=$filename ! 'video/x-raw, format=(string)NV12, framerate=(fraction)24/1, width=(int)1280, height=(int)720'  ! h265dec threads=1 !  vpe ! kmssink
