#!/bin/sh

machine_type="`cat /etc/hostname`"
filename="/usr/share/ti/video/airshow_p352x288.yuv"
output="$HOME/airshow_p352x288.h264"

if [ ! -f $filename ]; then
        echo "Video clip not found"
        exit 1
fi
echo ""
echo "Run H.264 Encoding on IVAHD"
echo "The encoded output will be saved to $HOME directory"
echo ""

gst-launch-1.0 filesrc location=$filename ! videoparse width=352 height=288 format=nv12 ! video/x-raw, width=352, height=288 ! ducatih264enc ! filesink location=$output
