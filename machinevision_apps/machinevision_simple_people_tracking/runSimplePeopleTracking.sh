#!/bin/sh

machine_type="`cat /etc/hostname`"
echo "Simple People Tracking TOF demo started!"
SimplePeopleTracking -w 160 -h 120 -f 30 & pid=$!
renice -n -19 -p $pid
for adjpri in $(pgrep weston)
do
   renice -n 19 -p $adjpri
done
