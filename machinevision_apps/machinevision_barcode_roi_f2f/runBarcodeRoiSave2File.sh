#!/bin/sh

machine_type="`cat /etc/hostname`"

input_image="/usr/share/ti/image/sample_barcode.jpg"
output_image="/usr/share/ti/image/barcode_detection.png"

if [ ! -f $input_image ]; then
        echo "Input barcode image not found!!!"
        exit 1
fi
echo ""
echo "Starting barcode detection..."
echo ""

detect_barcode $input_image 1 $output_image

if [ ! -f $output_image ]; then
        echo "Error: barcode detection incomplete!!!"
        exit 1
fi

echo "Barcode detection completed!"
