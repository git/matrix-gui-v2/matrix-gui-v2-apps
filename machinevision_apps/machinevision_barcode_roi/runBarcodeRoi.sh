#!/bin/sh

machine_type="`cat /etc/hostname`"

input_image="/usr/share/ti/image/sample_barcode.jpg"

if [ ! -f $input_image ]; then
        echo "Input barcode image not found!!!"
        exit 1
fi
echo ""
echo "Starting barcode detection..."
echo ""

ocl_acceleration=0

if [ "$machine_type" = "am57xx-evm" ]; then
  ocl_acceleration=1
fi

detect_barcode $input_image $ocl_acceleration 1 & pid=$!
{ sleep 20; kill $pid; }

echo "Barcode detection completed!"
