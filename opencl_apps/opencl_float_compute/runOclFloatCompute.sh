#!/bin/sh

echo ""
echo "Running OpenCL float_compute Example"
echo ""

cd /usr/share/ti/examples/opencl/float_compute
./float_compute

cd - > /dev/null
