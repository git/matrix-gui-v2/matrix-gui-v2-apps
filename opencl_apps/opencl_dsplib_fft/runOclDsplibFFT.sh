#!/bin/sh

echo ""
echo "Running OpenCL dsplib_fft Example"
echo ""

cd /usr/share/ti/examples/opencl/dsplib_fft
./dsplib_fft

cd -
