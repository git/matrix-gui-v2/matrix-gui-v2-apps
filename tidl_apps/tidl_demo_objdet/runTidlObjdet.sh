export numeve=$(/usr/share/ti/examples/opencl/platforms/platforms  | grep "Embedded Vision Engine" | wc -l)
export numdsp=$(find /proc/device-tree/ocp/ -name "dsp_system*" | wc -l)
cd /usr/share/ti/tidl/examples/ssd_multibox
echo "NUMEVEs is $numeve"
./ssd_multibox -p 40 -d 1 -e $numeve -f 1000 -i ./clips/pexels_videos_3623.mp4
