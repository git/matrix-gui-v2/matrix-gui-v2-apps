export numeve=$(/usr/share/ti/examples/opencl/platforms/platforms  | grep "Embedded Vision Engine" | wc -l)
export numdsp=$(find /proc/device-tree/ocp/ -name "dsp_system*" | wc -l)
export TIDL_NETWORK_HEAP_SIZE_EVE=56623104
export TIDL_NETWORK_HEAP_SIZE_DSP=56623104
echo "NUMEVEs is $numeve"
echo "NUMDSPs is $numdsp"
cd /usr/share/ti/tidl/examples/classification
if ((numeve > 0))
then
  ./tidl_classification -g 2 -d 1 -e $numeve -l ./imagenet.txt -s ./classlist.txt -i ./clips/test2.mp4 -c ./stream_config_j11_v2.txt
else
  ./tidl_classification -g 1 -d $numdsp -e 0 -l ./imagenet.txt -s ./classlist.txt -i ./clips/test2.mp4 -c ./stream_config_j11_v2.txt
fi
