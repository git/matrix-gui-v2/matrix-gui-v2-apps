numdsp=2
if cat /proc/device-tree/model | grep "AM571"
then
  numdsp=1
fi
echo "NUMDSPs is $numdsp"

cd /usr/share/ti/tidl/examples/classification

./tidl_classification -g 1 -d $numdsp -e 0 -l ./imagenet.txt -s ./classlist.txt -i ./clips/test10.mp4 -c ./stream_config_j11_v2.txt
