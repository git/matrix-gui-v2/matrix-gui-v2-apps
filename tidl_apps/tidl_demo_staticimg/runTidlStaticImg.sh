export numeve=$(/usr/share/ti/examples/opencl/platforms/platforms  | grep "Embedded Vision Engine" | wc -l)
export numdsp=$(find /proc/device-tree/ocp/ -name "dsp_system*" | wc -l)
export TIDL_NETWORK_HEAP_SIZE_EVE=56623104
export TIDL_NETWORK_HEAP_SIZE_DSP=56623104
cd /usr/share/ti/tidl/examples/classification
./tidl_classification -g 1 -d $numdsp -e $numeve -l ./imagenet.txt -s ./classlist.txt -i ./clips/test10.mp4 -c ./stream_config_j11_v2.txt
