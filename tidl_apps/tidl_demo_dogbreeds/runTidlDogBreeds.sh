cd /usr/share/ti/tidl/examples/classification
export numeve=$(/usr/share/ti/examples/opencl/platforms/platforms  | grep "Embedded Vision Engine" | wc -l)
export numdsp=$(find /proc/device-tree/ocp/ -name "dsp_system*" | wc -l)
echo "NUMEVEs is $numeve"
echo "NUMDSPs is $numdsp"
if ((numeve > 0))
then
  export TIDL_NETWORK_HEAP_SIZE_EVE=56623104
  export TIDL_NETWORK_HEAP_SIZE_DSP=56623104
  ./tidl_classification -g 2 -d 1 -e $numeve -l ./toydogsnet.txt -s ./toydogsclasses.txt -i ./clips/toydogs_854x480.mp4 -c ./stream_config_toydogs.txt
else
  ./tidl_classification -g 1 -d $numdsp -e 0 -l ./toydogsnet.txt -s ./toydogsclasses.txt -i ./clips/toydogs_854x480.mp4 -c ./stream_config_toydogs.txt
fi
