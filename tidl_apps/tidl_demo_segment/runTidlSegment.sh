export numeve=$(/usr/share/ti/examples/opencl/platforms/platforms  | grep "Embedded Vision Engine" | wc -l)
export numdsp=$(find /proc/device-tree/ocp/ -name "dsp_system*" | wc -l)
if ((numeve > 0))
then
  echo "NUMEVEs is $numeve"
  echo "NUMDSPs is $numdsp"
  cd /usr/share/ti/tidl/examples/segmentation
  ./segmentation -i ./clips/traffic_pixabay_298.mp4 -f 2000 -w 720
else
  echo "No EVEs available on this SoC"
fi
