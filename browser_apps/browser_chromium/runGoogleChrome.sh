#!/bin/sh

#Check PROXY_SERVER setting
#Example http=webproxy.ext.ti.com:80;https=webproxy.ext.ti.com:80

if [ PROXY_SERVER == "" ]
then
    google-chrome --log-level=3
else
    google-chrome --log-level=3 --proxy-server="$PROXY_SERVER"
fi
