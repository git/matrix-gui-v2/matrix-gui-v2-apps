#!/bin/bash

cd /usr/share/ti/examples/pdm

exe=RnnPdmAnomalyDetection

PID=`pidof $exe`

if [ "x${PID}x" != "xx" ]
then
    kill -9 ${PID}
fi

rm -rf /tmp/pdmfifo

# mean and std of the phase currents, to be used for normalization/denormalization
mean_current1=-0.0001841035315
mean_current2=0.004191935886
std_current1=0.01918238488
std_current2=0.01933241767
# relative ratio to determine the error threshold: max_err(calibration)*relative_threshold
relative_threshold=2
./$exe $mean_current1 $mean_current2 $std_current1 $std_current2 $relative_threshold &

PID=$!
echo "RnnPdmAnomalyDetection running as Process id: ${PID}"

# Stream the pre-recorded sensor data to the FIFO (/tmp/pdmfifo).
# /tmp/pdmfifo is consumed by the demo binary to have an identical consumer,
# while the writer to the fifo can be from any pre-recorded files or
# UART streaming.
cat ./normal100-anomaly150-normal100.log > /tmp/pdmfifo

while :
do
    PID=`pidof $exe`
    if [ "$PID" == "" ]
    then
       break
    else
       sleep 1
    fi
done

exit 0
