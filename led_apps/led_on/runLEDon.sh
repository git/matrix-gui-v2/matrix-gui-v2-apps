board_model=$(tr -d '\0' </proc/device-tree/model)

if [[ $board_model == *"AM642 EVM"* ]]
then
    test_led=am64-evm\:red\:heartbeat
elif [[ $board_model == *"AM642 SK"* ]]
then
    test_led=red\:activity
else
    exit
fi

echo none > /sys/class/leds/$test_led/trigger

echo 1 > /sys/class/leds/$test_led/brightness
