#!/bin/sh

OPENSSL=/usr/bin/openssl

cat /proc/cpuinfo | grep OMAP3 > /dev/null 2> /dev/null
if [ `echo $?` = "0" ]
then
	export CPU=OMAP3
else
	export CPU=other
fi

if [ -r $OPENSSL ]
then
	$OPENSSL version
else
	echo "Unable to find OpenSSL"
	exit 1
fi

echo "################################"
echo "Running OpenSSL Speed tests.  "

CRYPTO_PATH=`find / -name "crypto@*"`
PLATFORM=`tr -d '\0' < $CRYPTO_PATH/compatible`
TEMP=$HOME/temp
HARDWARE_PRESENT=`lsmod | grep -c sa2ul`

crypto_transform_j7=("aes-128-cbc" "aes-192-cbc" "aes-256-cbc" "aes-128-ecb" "aes-192-ecb" "aes-256-ecb" "des3" "sha1" "sha256" "sha512")
crypto_transform_am6=("aes-128-cbc" "aes-192-cbc" "aes-256-cbc" "aes-128-ecb" "aes-192-ecb" "aes-256-ecb" "sha256" "sha512")
crypto_transform_sw=("aes-128-cbc" "aes-192-cbc" "aes-256-cbc" "aes-128-ecb" "aes-192-ecb" "aes-256-ecb" "des3" "sha1" "sha224" "sha256" "sha386" "sha512" "md5" )

if [[ $HARDWARE_PRESENT -ne 0 ]];
then
	if [[ "$PLATFORM" == "ti,am64-sa2ul" || "$PLATFORM" == "ti,am62-sa3ul" ]];
	then
		echo "There are 9 hardware openssl tests and each takes 15 seconds..."
		for i in "${crypto_transform_am6[@]}"
		do
			echo "Running $i test.  Please Wait..."
			time -v $OPENSSL speed -evp "$i" -engine devcrypto -elapsed > $TEMP 2>&1
			egrep 'Doing|User|System|Percent|Elapsed' $TEMP
		done
	fi
	if [[ "$PLATFORM" == "ti,am654-sa2ul" || "$PLATFORM" == "ti,j721e-sa2ul" ]];
	then
		echo "There are 10 hardware openssl tests and each takes 15 seconds..."
		for i in "${crypto_transform_j7[@]}"
		do
			echo "Running $i test.  Please Wait..."
			time -v $OPENSSL speed -evp "$i" -engine devcrypto -elapsed > $TEMP 2>&1
			egrep 'Doing|User|System|Percent|Elapsed' $TEMP
		done
	fi
else
	echo "There are 13 software openssl tests and each takes 15 seconds..."
	for i in "${crypto_transform_sw[@]}"
	do
		echo "Running $i test.  Please Wait..."
		time -v $OPENSSL speed -evp "$i" -elapsed > $TEMP 2>&1
		egrep 'Doing|User|System|Percent|Elapsed' $TEMP
	done
fi

rm $TEMP
