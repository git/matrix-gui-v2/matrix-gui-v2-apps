<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Keystone II Utility Application System Information</title>

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<script type="text/javascript" src="/javascript/jquery-latest.js"></script>
<link rel='stylesheet' type='text/css' href='/css/global.css'>

</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>Information</h1></div>
<div id='header'>
<div id='menu'>
</div>

<div id='page'>
<div id='content'>
<hr>
<table cellpadding='6'>
<tr>
<td valign='center'><image src='/apps/images/keystone-c66x.png'></td>
<td width='100%' align='justify'>
<h2>Information</h2><br>
<p class='Description'><b>Description:</b> This page displays the static system information. </p>
</td>
</tr>
</table>
<hr>
</div>
<br>

<div>
<h3>System Up Time</h3>
<PRE>
<?php
	$output = shell_exec('cat /proc/uptime');
	echo "$output";
?>
</PRE>
</div><br>

<div>
<h3>CPU Info</h3>
<PRE>
<?php
	$output = shell_exec('cat /proc/cpuinfo');
	echo "$output";
?>
</PRE>
</div><br>

<div>
<h3>Linux version</h3>
<PRE>
<?php
	$output = shell_exec('uname -srm');
	echo "$output";
?>
</PRE>
</div><br>

<div>
<h3>Mount Info</h3>
<PRE>
<?php
	$tmpfile = shell_exec('mktemp');
	shell_exec("mount > $tmpfile");
	$output = shell_exec("(mount | awk ' {print \"device \", $1,\" mounted on \",$3, \" as \", $5, \"filesystem\"}')");
	echo "$output";
?>
</PRE>
</div><br>

<div>
<h3>Network Interfaces</h3>
<PRE>
<?php
	$output = shell_exec('ifconfig');
	echo "$output";
?>
</PRE>
</div><br>

<div>
<h3>Routing table</h3>
<PRE>
<?php
	$output = shell_exec('route');
	echo "$output";
?>
</PRE>
</div><br>

<div>
<h3>Module Info</h3>
<PRE>
<?php
	$output = shell_exec('lsmod');
	echo "$output";
?>
</PRE>
</div><br>

<hr>
<div style='clear: both;'>&nbsp;</div>
</div>
<div id="footer">
	<p id="legal">( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</div>
</body>
</html>
