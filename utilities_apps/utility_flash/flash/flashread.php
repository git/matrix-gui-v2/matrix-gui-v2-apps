<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<link rel='stylesheet' type='text/css' href='/css/global.css'>

</head>
<body>
<div id='wrapper'><div id='logo'><h1>Flash Read</h1>
</div><div id='header'><div id='menu'><ul>
</ul></div></div></div>
<div id='page'>

<?php
	$partition = $_POST['mtd_partition'];
	$mtddevname = shell_exec("(cat /proc/mtd | grep \\\"$partition\\\" | cut -d: -f1)");
	$mtddev = "/dev/".$mtddevname;
	$image_name = "/tmp/partition.bin";

	if ($mtddevname == "") {
		echo "<p>Could not find Nand partition for $partition ...</p>";
	} else {
		$mtdsz = shell_exec("cat /proc/mtd | grep \\\"$partition\\\" | cut -d: -f2 | awk '{printf $1}'");
		$mtdsz = "0x".$mtdsz;
		echo "<p>Reading $mtdsz bytes from $mtddev ...</p>";
		shell_exec("nanddump -q -f $image_name -l $mtdsz $mtddev");
		shell_exec("ln -s $image_name partition.bin");
	}
?>

<div id='content'>
<input type="button" name="Button" value="Save NAND data" onClick="window.open('partition.bin', 'download'); return false;">
</div>

<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
