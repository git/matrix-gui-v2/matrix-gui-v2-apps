<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<link rel='stylesheet' type='text/css' href='/css/global.css'>

</head>

<body>
<div id='wrapper'><div id='logo'><h1>Add File</h1>
</div><div id='header'><div id='menu'><ul>
</ul></div></div></div>
<div id='page'>

<?php
	$file_name = $_POST['rFile'];
	$data_tmp_file = $_FILES['lFile']['tmp_name'];
	$fsize = $_FILES['lFile']['size'];

	if ($fsize == 0) {
		echo "<p>Bad Input file ...</p>";
	} elseif ($file_name == "") {
		echo "<p>Remote file not specified ...</p>";
	} else {
		$directory = dirname($file_name);
		if (file_exists($directory) == FALSE) {
			echo "<p>Could not find the path $directory ...</p>";
		} else {
			echo "<p>Writing $fsize bytes to $file_name ...</p>";
			shell_exec("cp $data_tmp_file $file_name");
			shell_exec('sync');
			echo "<p>Write to $file_name complete ...</p>";
		}
	}
?>

<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
