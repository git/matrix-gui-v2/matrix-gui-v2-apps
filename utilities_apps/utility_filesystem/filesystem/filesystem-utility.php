<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Keystone II Utility Applications</title>
<META HTTP-EQUIV="REFRESH" CONTENT="60">

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<link rel='stylesheet' type='text/css' href='/css/global.css'>
</head>
<body>
<div id="wrapper">
	<div id="logo">
		<h1>Filesystem Utility</h1>
	</div>
	<div id="header">
		<div id="menu">
		</div>
	</div>
</div>

<div id="page">
	<div id="content">
		<hr>
		<table cellpadding="6">
			<tr><td valign="center">
			<image src="/apps/images/keystone-c66x.png">
			</td><td width="100%" align="justify">
			<h2>Write to filesystem on the Platform</h2>
			<br>
			<p class="Description"><b>Description:</b> This page allows the user to add file to filesystem.</p>
			</td></tr>
		</table>

		<hr>
        <br>

		<div id="filesystemwrite" style="width: 90%; margin-left: 25px; padding: 2px; border: 1px gray solid;">
			<form action="/html-apps/filesystem/filesystemwrite.php" enctype="multipart/form-data" method="post">
			<table border="0" cellpadding="5" id="filesystemwritetable" width="100%">
				<caption> <b> File Write </b> </caption>
				<tr>
					<td width="20%" align="left">Remote File Name </td>
					<td align="left" width="60%">
					<input type="text"  name="rFile" size="48" value=""></td>
				</tr>
				<tr>
					<td width="20%" align="left">Local File</td>
					<td align="left" width="60%">
					<input type="file"  name="lFile" size="40px" value=""></td>
				</tr>
				<tr>
					<td width="20%" align="left" colspan="2">
					<input type="submit" value="Write File"></td>
				</tr>
			</table>
			</form>
		</div>
		<br>
		<br>
		<br>
	</div>

	<div style="clear: both;">&nbsp;</div>
</div>

<div id="footer">
	<p id="legal">( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</div>

</body>
</html>
