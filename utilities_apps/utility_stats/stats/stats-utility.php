<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Keystone II Utility Application System Statistics</title>
<META HTTP-EQUIV="REFRESH" CONTENT="60">

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<script type="text/javascript" src="/javascript/jquery-latest.js"></script>
<link rel='stylesheet' type='text/css' href='/css/global.css'>

</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>System Statistics</h1></div>
<div id='header'>
<div id='menu'>
</div>

<div id='page'>
<div id='content'>
<hr><table cellpadding='6'><tr>
<td valign='center'><image src='/apps/images/keystone-c66x.png'></td>
<td width='100%' align='justify'>
<h2>Statistics</h2><br>
<p class='Description'><b>Description:</b> This page displays system statistics like Memory and CPU usage. This page refreshes itself every 60 seconds.</p>
</td></tr></table><hr><br>
</div>

<div>
<h3>Memory & CPU Usage</h3>
<PRE>
<?php
	$tmpfile = shell_exec('mktemp');
	shell_exec("top -bn1 > $tmpfile");
	shell_exec("sed '1,10!d' $tmpfile");
	$output = shell_exec("cat $tmpfile");
	echo "$output";
?>

<hr>
<div style="clear: both;">&nbsp;</div>
</div>
<div id="footer">
	<p id="legal">( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</div>

</body>
</html>
