<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Keystone II Utility Applications</title>
<META HTTP-EQUIV="REFRESH" CONTENT="60">

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<link rel='stylesheet' type='text/css' href='/css/global.css'>

<SCRIPT TYPE="text/javascript">
	function openpopup(url)
	{
	    location.href=url;
	}
	function submitForm1()
	{
		if(document.pressed == 'Write EEPROM')
		{
			document.eepromWrite.action ="/html-apps/eeprom/eepromwrite.php";
		}
		return true;
	}

	function submitForm2()
	{
		if(document.pressed == 'Read EEPROM')
		{
			document.eepromRead.action ="/html-apps/eeprom/eepromread.php";
		}
		return true;
	}

</SCRIPT>
</head>
<body>
<div id="wrapper">
	<div id="logo">
		<h1>EEPROM Utility</h1>
	</div>
	<div id="header">
		<div id="menu">
		</div>
	</div>
</div>

<div id="page">
	<div id="content">
		<hr>
		<table cellpadding="6">
			<tr><td valign="center">
			<image src="/apps/images/keystone-c66x.png">
			</td><td width="100%" align="justify">
			<h2>Program the EEPROM on the Platform</h2>
			<br>
			<p class="Description"><b>Description:</b> This page allows the user to read/write the I2C EEPROM.</p>
			</td></tr>
		</table>
		<hr>
		<br>
		<div id="eepromWrite" style="width: 90%; margin-left: 25px; padding: 2px; border: 1px gray solid;">
			<form name="eepromWrite" onsubmit="return submitForm1();" enctype="multipart/form-data" method="post">
			<table border="0" cellpadding="5" id="eepromwritetable" width="100%">
				<caption> <b> I2C EEPROM Write </b> </caption>
				<tr>
				<td width="30%" align="left">I2C Bus Address</td>
				<td align="left" width="60%">
				<input type="radio" value="0x50" name="busAddr"> 0x50
				<input type="radio" value="0x51" checked name="busAddr"> 0x51</td>
				</tr>
				<tr>
				<td width="30%" align="left">Image File</td>
				<td align="left" width="60%">
				<input type="file"  name="datafile" size="40px" value=""></td>
				</tr>
				<tr>
				<td width="91%" align="left" colspan="2"><input type="submit" onclick="document.pressed=this.value" value="Write EEPROM"></td>
				</tr>
			</table>
			</form>
		</div>
		<br>
		<div id="eepromRead" style="width: 90%; margin-left: 25px; padding: 2px; border: 1px gray solid;">
			<form name="eepromRead" onsubmit="return submitForm2();" enctype="multipart/form-data" method="post">
			<table border="0" cellpadding="5" id="eepromreadtable" width="100%">
				<caption> <b> I2C EEPROM Read </b> </caption>
				<tr>
				<td width="30%" align="left">I2C Bus Address</td>
				<td align="left" width="60%">
				<input type="radio" value="0x50" name="busAddr"> 0x50
				<input type="radio" value="0x51" checked name="busAddr"> 0x51</td>
				</tr>
				<tr>
				<td width="30%" align="left"><input type="submit" value="Read EEPROM" onclick="document.pressed=this.value"> </td>
				</tr>
			</table>
			</form>
		</div>
		<br>
		<br>
		<br>
	</div>

	<div style="clear: both;">&nbsp;</div>
</div>

<div id="footer">
	<p id="legal">( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</div>

</body>
</html>
