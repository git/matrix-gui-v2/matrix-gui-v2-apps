<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<link rel='stylesheet' type='text/css' href='/css/global.css'>

</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>EEPROM Read</h1></div>
<div id='header'>
<div id='menu'>
</div>
</div>
</div>
<div id='page'>

<?php
	$bus_addr = $_POST['busAddr'];
	$eepromdev = shell_exec('find /sys -name eeprom');
	$eepromSz = 65536;
	$readSz = $eepromSz;
	$image_name = "/tmp/eeprom.bin";

	if ($bus_addr == 0x51) {
		$devOffset = $eepromSz;
	} else {
		$devOffset = 0;
	}

	echo "<p>Reading $readSz bytes from EEPROM Bus Address $bus_addr ...</p>";
	shell_exec("dd bs=1 count=$readSz skip=$devOffset of=$image_name if=$eepromdev");
	shell_exec("ln -s $image_name eeprom.bin");
?>

<div id='content'>
<input type="button" name="Button" value="Save EEPROM data" onClick="window.open('eeprom.bin', 'download'); return false;">
</div>

<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
