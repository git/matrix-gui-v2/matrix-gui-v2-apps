<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/css/fonts-min.css">
<link rel='stylesheet' type='text/css' href='/css/global.css'>

</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>EEPROM Write</h1></div>
<div id='header'>
<div id='menu'>
</div>
</div>
</div>
<div id='page'>

<?php
	$bus_addr = $_POST['busAddr'];
	$data_tmp_file = $_FILES['datafile']['tmp_name'];
	//shell_exec("cp $data_tmp_file ./data.bin");

	$eepromdev = shell_exec('find /sys -name eeprom');
	$fsize=$_FILES['datafile']['size'];
	$eepromSz = 65536;

	if ($bus_addr == 0x51) {
		$devOffset = $eepromSz;
	} else {
		$devOffset = 0;
	}

	if ($fsize == 0) {
		echo "<p>Bad Input file ...</p>";
	} elseif ($fsize > $eepromSz) {
		echo "<p>Data file size greater than $eepromSz bytes ...</p>";
	} else {
		echo "<p>Writing $fsize bytes to EEPROM Bus Address $bus_addr ...</p>";
		shell_exec("dd bs=1 count=$fsize seek=$devOffset if=$data_tmp_file of=$eepromdev");
		echo "<p>EEPROM programming completed!</p>";
	}
?>

<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2015 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
